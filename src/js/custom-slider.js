function css(element, styles) {
	for (const key in styles) {
		element.style[key] = styles[key];
	}
}

export default class CustomSlider {
	constructor (selector) {
		this.selector = selector;
		this.sliderElement = document.querySelector(selector);
		this.sliderWrapElement = this.sliderElement.querySelector('.custom-slider__wrapper');
		this.nextButton = this.sliderElement.querySelector('.custom-slider__button-next');
		this.prevButton = this.sliderElement.querySelector('.custom-slider__button-prev');

		this.countSlides = this.sliderElement.querySelectorAll('.custom-slider__slide').length;

		this.containerWidth = this.sliderElement.offsetWidth; // ширина слайдера
		this.widthActiveSlide = 0.86 * this.containerWidth; // ширина активного слайда
		this.containerHeight = 0.60 * this.widthActiveSlide; // высота активного слайда и слайдера

		css(this.sliderElement, { height: this.containerHeight + 'px' });
	}

	/*
		Клонируем все слайды чтобы минимум было 5 штук, для сглаживания переходов
		Слайдам расставляем индексы
		Раздаем классы и устанавливаем положение
		Добавляем нужных слушателей событий (кнопки)
	*/
	init() {
		let slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		for( const indexSlide in [...slides] ) {
			let clone = slides[indexSlide].cloneNode(true);

			slides[indexSlide].dataset.index = parseInt(indexSlide) + 1;
			css(slides[indexSlide], {
				width: this.widthActiveSlide + 'px',
				height: this.containerHeight + 'px'
			});

			clone.dataset.index = parseInt(indexSlide) + this.countSlides + 1;
			clone.classList.add('custom-slide-clone');
			css(clone, {
				width: this.widthActiveSlide + 'px',
				height: this.containerHeight + 'px'
			});
			this.sliderWrapElement.append(clone);
		}

		slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.sliderWrapElement.prepend(slides[slides.length - 1]);
		this.sliderWrapElement.prepend(slides[slides.length - 2]);

		this.setClasses();

		this.setPosition();

		this.nextButton.addEventListener('click', () => { this.nextSlide(); });
		this.prevButton.addEventListener('click', () => { this.prevSlide(); });

		window.addEventListener('resize', () => { this.resizeSlider(); });

		const swipe = new Hammer(this.sliderElement);

		swipe.on('swipeleft swiperight', ev => {
			if(ev.type === 'swipeleft') {
				this.nextSlide();
			} else {
				this.prevSlide();
			}
		});
	}

	resizeSlider() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.containerWidth = this.sliderElement.offsetWidth;
		this.widthActiveSlide = 0.86 * this.containerWidth;
		this.containerHeight = 0.68 * this.widthActiveSlide;

		for( const indexSlide in [...slides] ) {
			css(slides[indexSlide], {
				width: this.widthActiveSlide + 'px',
				height: this.containerHeight + 'px'
			});
		}

		css(this.sliderElement, { height: this.containerHeight + 'px' });

		this.setPosition();
	}

	// установка классов активным слайдам
	setClasses() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		for( const indexSlide in [...slides] ) {
			slides[indexSlide].classList.remove('custom-slider__slide-prev', 'custom-slider__slide-next', 'custom-slider__slide-active');
		}

		this.sliderElement.querySelector(`.custom-slider__slide:nth-child(2)`).classList.add('custom-slider__slide-prev');
		this.sliderElement.querySelector(`.custom-slider__slide:nth-child(3)`).classList.add('custom-slider__slide-active');
		this.sliderElement.querySelector(`.custom-slider__slide:nth-child(4)`).classList.add('custom-slider__slide-next');
	}

	// установка положения видимых слайдов
	setPosition() {
		if(window.matchMedia('(max-width: 1024px)').matches) {
			this.setMobilePosition();
		} else {
			this.setDesktopPosition();
		}
	}

	// установка положения активным слайдам в мобильной версии
	setMobilePosition() {
		const prev2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(1)`);
		const prev1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(2)`);
		const activeSlide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(3)`);
		const next1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(4)`);
		const next2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(5)`);

		css(prev2Slide, {
			transform: `translate3d(${-0.08 * this.widthActiveSlide}px, 0, -100px) scale(0.84);`,
			opacity: 0
		});

		css(prev1Slide, {
			transform: `translate3d(${-0.08 * this.widthActiveSlide}px, 0, -100px) scale(0.84)`,
			opacity: .6
		});

		css(activeSlide, {
			transform: `translate3d(${(this.containerWidth - this.widthActiveSlide) / 2}px, 0, 0) scale(1)`,
			opacity: 1
		});

		css(next1Slide, {
			transform: `translate3d(${(this.containerWidth - this.widthActiveSlide + 0.08 * this.widthActiveSlide)}px, 0, -100px) scale(0.84)`,
			opacity: .6
		});

		css(next2Slide, {
			transform: `translate3d(${(this.containerWidth - this.widthActiveSlide + 0.08 * this.widthActiveSlide)}px, 0, -100px) scale(0.84)`,
			opacity: 0
		});
	}

	// установка положения активным слайдам в десктоп версии
	setDesktopPosition() {
		const prev1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(2)`);
		const activeSlide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(3)`);
		const next1Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(4)`);
		const next2Slide = this.sliderElement.querySelector(`.custom-slider__slide:nth-child(5)`);

		css(prev1Slide, {
			transform: `translate3d(
										${this.containerWidth - (0.95 * this.widthActiveSlide) - (0.25 * 0.5 * this.widthActiveSlide)}px, 
										${0.05 * this.containerHeight}px, 
										-150px
									) 
									scale(0.9)`,
			opacity: 0
		});

		css(activeSlide, {
			transform: `translate3d(
										${-0.05 * this.widthActiveSlide}px, 
										${-0.05 * this.containerHeight}px, 
										0
									) 
									scale(0.9)`,
			opacity: 1
		});

		css(next1Slide, {
			transform: `translate3d(
										${(this.containerWidth - this.widthActiveSlide + 0.25 * this.widthActiveSlide)}px, 
										${(0.5 * this.containerHeight) - (0.5 * this.containerHeight)}px, 
										-100px
									) 
									scale(0.5)`,
			opacity: 1
		});

		css(next2Slide, {
			transform: `translate3d(
										${this.containerWidth - (0.95 * this.widthActiveSlide) - (0.25 * 0.5 * this.widthActiveSlide)}px, 
										${0.05 * this.containerHeight}px, 
										-150px
									) 
									scale(0.9)`,
			opacity: 1
		});
	}

	// следующий слайд
	nextSlide() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.sliderWrapElement.append(slides[0])

		if(this.currentIndex + 1 > (2 * this.countSlides)) {
			this.currentIndex = 1;
		} else {
			this.currentIndex++;
		}

		this.setClasses();
		this.setPosition();
	}

	// предыдущий слайд
	prevSlide() {
		const slides = this.sliderElement.querySelectorAll('.custom-slider__slide');

		this.sliderWrapElement.prepend(slides[(2 * this.countSlides) - 1]);

		if(this.currentIndex - 1 < 1) {
			this.currentIndex = 2 * this.countSlides;
		} else {
			this.currentIndex--;
		}

		this.setClasses();
		this.setPosition();
	}
}