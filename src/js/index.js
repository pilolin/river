import tippy from 'tippy.js';
import { Swiper, EffectCoverflow, Navigation, EffectFade } from 'swiper/js/swiper.esm.js';
import 'hammerjs';
import CustomSlider from './custom-slider';
import Swal from 'sweetalert2/dist/sweetalert2.js'
import smoothscroll from 'smoothscroll-polyfill';
smoothscroll.polyfill();
import './../scss/styles.scss';

Swiper.use([Swiper, EffectCoverflow, Navigation, EffectFade]);

const swalModal = Swal.mixin({
	showConfirmButton: false,
	showCloseButton: true,
	closeButtonHtml: '<i class="icon-close" aria-label="Close modal" data-modal-close></i>',
	showClass: {
		popup: 'animated fadeInUp fast'
	},
	hideClass: {
		popup: 'animated fadeOutUp fast'
	}
})

// выезжающие элементы
document.addEventListener('click', function(e) {
	const target = e.target;

	if( !(target.closest('a') && target.closest('a').dataset.rollOpen) ) return 0;

	const rollId =  target.closest('a').dataset.rollOpen;
	closeRoll();
	target.closest('a').classList.add('active');
	openRoll(rollId);

	e.preventDefault();
});

document.addEventListener('click', function(e) {
	const target = e.target;

	if( !target.closest('.roll__close') )  return 0;

	closeRoll();

	e.preventDefault();
});

function setHeightRollContent(rollId) {
	const rollElement = document.getElementById(rollId);
	const rollContainer = rollElement.querySelector('.roll__container');
	const rollHeader = rollElement.querySelector('.roll__header');
	const rollContent = rollElement.querySelector('.roll__content');
	const heightHeaderBar = document.querySelector('.m-header-bar').clientHeight

	rollContainer.style.height = window.innerHeight - heightHeaderBar + 'px';
	rollContent.style.maxHeight = rollContainer.clientHeight - rollHeader.clientHeight - 15 - 24 + 'px';

}

function openRoll(rollId) {
	const rollElement = document.getElementById(rollId);

	setHeightRollContent(rollId);
	rollElement.classList.add('opened');
	backlightNavbarItems();
	rollElement.style.transform = `translate3d(0px, -${window.innerHeight}px, 0px)`;
	setTimeout(hideScroll, 50);
}

function closeRoll() {
	const rollElement = document.querySelector('.roll.opened');

	if( rollElement ) {
		rollElement.classList.remove('opened');
		rollElement.style.transform = 'translate3d(0px, 0px, 0px)';
		backlightNavbarItems();
	}
	setTimeout(showScroll, 50);
}

// блокировка/разблокировка скрола
function hideScroll() {
	setTimeout(() => {
		if ( !document.body.hasAttribute('data-body-scroll-fix') ) {
			let scrollPosition = window.pageYOffset || document.documentElement.scrollTop;
			document.body.setAttribute('data-body-scroll-fix', scrollPosition);
			document.body.style.overflow = 'hidden';
			document.body.style.position = 'fixed';
			document.body.style.top = '-' + scrollPosition + 'px';
			document.body.style.left = '0';
			document.body.style.width = '100%';
		}
	}, 10 );
}

function showScroll() {
	if ( document.body.hasAttribute('data-body-scroll-fix') ) {
		const scrollPosition = document.body.getAttribute('data-body-scroll-fix');
		document.body.removeAttribute('data-body-scroll-fix');
		document.body.style.overflow = '';
		document.body.style.position = '';
		document.body.style.top = '';
		document.body.style.left = '';
		document.body.style.width = '';
		window.scroll(0, scrollPosition);
	}
}

// всплывашки на именах героев с фото
tippy('a[href^="#"][data-tippy-img]', {
	trigger: "mouseenter focus click",
	arrow: false,
	placement: 'right-start',
	content(ref) {
		return `
			<div class="tooltip-actor-photo">
				<img src="${ref.dataset.tippyImg}" alt="${ref.innerText}">
				<div class="caption-18">${ref.innerText}</div>
			</div>`
	}
});

// табы
document.addEventListener('click', function(e) {
	const target = e.target;

	if( target.parentElement.getAttribute('role') !== 'tab' ) return 0;

	const tabsContainer = target.closest('.tabs');
	const clickedTab = target.closest('.tabs-list__item');
	const contentForView = tabsContainer.querySelector(clickedTab.firstChild.hash);

	tabsContainer.querySelector('.tabs-list__item_active').classList.remove('tabs-list__item_active');
	tabsContainer.querySelector('.tabs-content-wrap_active').classList.remove('tabs-content-wrap_active');

	clickedTab.classList.add('tabs-list__item_active');
	contentForView.classList.add('tabs-content-wrap_active');

	e.preventDefault();
});

// рейтинг
document.addEventListener('mouseover', function(e) {
	const target = e.target;

	if( target.closest('.rating__star') && !isTouchDevice()) {
		const ratingWrap = target.closest('.rating-wrap');
		const currentStar = target.closest('.rating__star');
		const listStar = ratingWrap.children;
		let hoverStar = true;

		for (var star of [...listStar]) {
			if(hoverStar) {
				star.classList.add('rating__star_hover');
			} else {
				star.classList.remove('rating__star_hover');
			}

			if( hoverStar && star === currentStar ) {
				hoverStar = false;
			}
		}
	}
});

document.addEventListener('mouseout', function(e) {
	const target = e.target;

	if( target.closest('.rating') && !isTouchDevice()) {
		const ratingWrap = target.closest('.rating-wrap');
		const listStar = ratingWrap.children;

		for (var star of [...listStar]) {
			star.classList.remove('rating__star_hover');
		}
	}
});

// проверка на touch устройство
function isTouchDevice() {
	return 'ontouchstart' in window;
}

// озвучки в эпизоде
function initSoundsTabs() {
	const sounds = new Swiper('.episode-sounds .tabs-list', {
		init: false,
		slidesPerView: 'auto',
		spaceBetween: 37,
		watchOverflow: true
	});

	sounds.on('init', () => {
		const tabs = sounds.el.querySelectorAll('.tabs-list__item');
		const widthSoundsTabs = [...tabs].reduce((total, tab) => total + tab.offsetWidth, 0) + (sounds.params.spaceBetween * (tabs.length - 1));

		if( widthSoundsTabs > sounds.el.offsetWidth) {
			tabs[tabs.length - 1].parentElement.style.width = '100%';
		} else {
			tabs[tabs.length - 1].parentElement.style.width = 'auto';
		}
		sounds.update();
	});

	sounds.init();

	document.addEventListener('click', e => {
		if(e.target.closest('.tabs-list__item')) {
			const target = e.target.closest('.tabs-list__item');
			const tabsSlider = target.closest('.tabs-list__slides')
			const tabsList = tabsSlider.querySelectorAll('.tabs-list__item');

			sounds.slideTo([...tabsList].indexOf(target));
		}
	})

}

// пагинация
function initMobilePagination() {
	new Swiper('.pagination', {
		effect: 'coverflow',
		centeredSlides: true,
		slidesPerView: 5,
		coverflowEffect: {
			rotate: 0,
			stretch: -13,
			depth: 100,
			modifier: 1,
			slideShadows: false,
		},
		breakpoints: {
			768: {
				scoverflowEffect: {
					stretch: -35,
				},
			}
		}
	})
}

document.addEventListener('DOMContentLoaded', () => {
	if(isTouchDevice()) {
		document.body.classList.add('touch');
	} else {
		document.body.classList.add('no-touch');
	}
});

window.addEventListener('load', () => {
	if(document.querySelector('.episode-sounds .tabs-list')) {
		initSoundsTabs();
	}

	if(isTouchDevice()) {
		initMobilePagination();
	}
});

if(document.getElementById('preview-slider')) {
	window.addEventListener('load', () => {
		new CustomSlider('#preview-slider').init();
	});
}

document.addEventListener('click', (e) => {
	const target = e.target;

	if( !(target.closest('a, button') && target.closest('a, button').dataset.modalOpen) ) return 0;

	const modalId = target.closest('a, button').dataset.modalOpen;

	swalModal.fire({
		html: document.getElementById(modalId).innerHTML,
		onOpen: (currentModal) => {
			const offsetTop = (window.innerHeight - currentModal.clientHeight) / 2;
			const buttonClose = currentModal.querySelector('.swal2-close');

			buttonClose.style.top = -(offsetTop / 2) + 'px';
		}
	});

	e.preventDefault();
})

// плавный скрол к якорю
document.addEventListener('click', e => {
	const link = e.target.closest('a');

	if( !link ) return 0;

	const linkHash = link.getAttribute('href').replace(/^(\/)/, '');

	if(linkHash[0] === '#' && linkHash.length > 1 && link.parentElement.getAttribute('role') !== 'tab') {

		if( link.getAttribute('href')[0] === '/' && link.getAttribute('href').indexOf(window.location.pathname) === -1 ) return 0;

		closeRoll();
		setTimeout(() => {
			scrollPageToElement(linkHash);
		}, 100);
		e.preventDefault();
	}
});

window.addEventListener('load', () => {
	if(window.location.hash.length > 1) {
		scrollPageToElement(window.location.hash);
	}
})

function scrollPageToElement(idElement) {
	const scroll = document.querySelector(idElement).offsetTop;

	window.scrollTo({top: scroll, behavior: 'smooth'});
}

//галерея увеличенная
document.addEventListener('click', (e) => {
	const target = e.target;

	if( !target.closest('.custom-slider__slide-active') ) return 0;

	const slider = target.closest('.custom-slider-container');
	const slides = slider.querySelectorAll('.custom-slider__slide');
	const modal = document.getElementById('modal-gallery');
	const modalWrap = modal.querySelector('.swiper-wrapper');
	const realCountSlides = slides.length / 2;
	let modalSlides = '';

	for (let i = 2; i < realCountSlides + 2; i++) {
		const img = slides[i].querySelector('img').getAttribute('src');

		modalSlides += `<div class="swiper-slide" style="background-image: url(${img})"></div>`;
	}

	modalWrap.innerHTML = modalSlides;

	swalModal.fire({
		html: modal.innerHTML,
		customClass: {
			popup: 'swal2-gallery'
		},
		onOpen: (currentModal) => {
			new Swiper('.swal2-container .swiper-container', {
				loop: true,
				effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});

			const offsetTop = (window.innerHeight - currentModal.clientHeight) / 2;
			const buttonClose = currentModal.querySelector('.swal2-close');

			buttonClose.style.top = -(offsetTop / 2) + 'px';
		}
	});
});

// подсветка нужных пунктов меню
const anchorAboutSerial = document.querySelector('.m-header-bar a[href="/#about-serial"]');
const elemAboutSerial = document.getElementById('about-serial');

if( anchorAboutSerial ) {
	window.addEventListener('load', backlightNavbarItems);
	window.addEventListener('scroll', backlightNavbarItems);
}

function backlightNavbarItems() {
	offBacklight();
	if(document.querySelector('.roll.opened')) {

		const rollOpened = document.querySelector('.roll.opened');
		const link = document.querySelector(`a[data-roll-open="${rollOpened.getAttribute('id')}"]`);

		onBacklight(link);

	}	else if( elemAboutSerial && elemAboutSerial.offsetTop < (window.scrollY + (window.innerHeight / 2)) 
		&& (elemAboutSerial.offsetTop + elemAboutSerial.clientHeight) > (window.scrollY + (window.innerHeight / 2)) ) {

		onBacklight(anchorAboutSerial);

	} else {

		homeBacklight();

	}
}

const homeBacklight = () => {
	if(window.location.pathname === '/') {
		onBacklight(document.querySelector('.m-header-bar a[href="/"]'));
	}
}

const onBacklight = element => {
	if(element && !element.classList.contains('active')) {
		offBacklight();
		element.classList.add('active');
	}
}

const offBacklight = () => {
	const activeElement = document.querySelector('.m-header-bar a.active');
	if(activeElement && activeElement.classList.contains('active')) {
		activeElement.classList.remove('active');
	}
}